歷代信條 - 亞他那修信經研究
===========================

亞他那修信經的起源
------------------

#### 亞歷山大主教亞他那修所作

亞他那修(293 - 373)為四世紀埃及亞歷山大主教，其生平最為人知道的，就是力抗亞流派異端，後人敬稱為**正統信仰之父**。但十七世紀羅馬天主教學者 G. J. Voss 反對是亞他那修所作。

反對原因如下 :

1. 亞他那修用希臘文寫作；但亞他那修信經原初的形式，是用拉丁文寫成 [[1]]
2. 亞他那修的神學主張是基督與聖父**本體相同 (homoousios)**，但亞他那修信經並無此用語。 [[1]]
3. 亞他那修與他的著作中都沒有表明此信經的存在 [[2]]
4. 信經中的神學思想發展於亞他那修教父之後的年代 [[2]]

[1]: http://www.ccel.org/contrib/cn/creeds/athancreed.html
[2]: http://en.wikipedia.org/wiki/Athanasian_Creed#Origin

#### 第五世紀神學家所做

後人推測可能是五世紀高盧 (法國) Lérins 修道院的教父 Vincent 所作。(Wikipedia, Encyclopaedia Britannica) [[3]]

其因如下：

1. 此信經流傳時期約從五世紀末到六世紀初
2. 一開始是從高盧地區西方教會流傳出來的，東方教會直到十二世紀才知道有這信經存在
3. 此信經所用的語彙與 Vincent 教父之著作有相同之處
4. 此信經顯然是基於奧古斯丁的三位一體論來闡述三位一體教義

[3]: http://global.britannica.com/EBchecked/topic/40585/Athanasian-Creed

#### 結論

關於此信經的作者有許多的推論，但此信經的神學內容顯然是基於奧古斯丁的三位一體論，加上流傳時間點，可推測此信經應是五世紀左右由西方教會的神學家所作。

亞他那修信經的價值
------------------

#### 教會內使用

* 教會崇拜用
* 教育信徒
* 防備異端
    1. 撒伯流

	    撒伯流派是早期的神格一位論異端，認為沒有三位一體，只有一位上帝，是同一主體。 [[1]]

    2. 阿波林

	    阿波林 (310-390) 是亞他那修的好朋友，但在對抗亞流派異端過程中，阿波林反走向另一個極端，認為基督的神人二性中，並無人的靈魂，只有人的肉身。換句話說，基督的人性只有肉身，並無人的靈魂。因此阿波林在主後 381 年康士坦丁會議中被定為異端。 [[2]]
    
    3. 亞流派

	    亞流派異端是早期教會最大的異端，認為基督是被造的，故不應當受敬拜。 [[3]]
    
    4. 猶提乾
    
	    猶提乾異端，主張基督並無神人二性，只有一性，因此又稱「基督一性論」，因神性與人性聯合後，人性如同一滴水滴在如海一般的神性本體中，人性便消失。 [[4]]
    
[1]: http://www.sekiong.net/ASS-CH/CH21.htm
[2]: http://tw.myblog.yahoo.com/lk1591q/article?mid=27505
[3]: http://www.sekiong.net/ASS-CH/CH18.htm
[4]: http://herewestand.org/blog/2013/07/09/belgicconfessionchristhumannature/


#### 神學思想的價值 [[1]]

* 最早，且最好的三位一體信條

  亞他那修信經是第一個陳述三位一體教義的信經，並且是最好的一個。

* 三大正統信仰告白

  文詞簡潔、清晰，是西方三大正統信仰告白之一。

* 顯示教會歷史變遷

  如今只流傳于天主教及英國國教聖公會之中。因為宗教改革後的教會因其第一句與最後一句咒詛性的信條，而不使用此信經。
  
[1]: http://www.ccel.org/contrib/cn/creeds/athancreed.html

結論
----

個人認為雖然近代教會否定了亞他那修信經，但我認為首末兩句是必須認真思考，一位信徒若不明白三一真神最基本的關係與地位，便會對基督的救贖功效上沒有全然的信心，終究必使信徒走向危機。
